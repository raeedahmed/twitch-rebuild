import sqlite3
from concurrent.futures.thread import ThreadPoolExecutor
from configparser import ConfigParser
from datetime import datetime, timezone
from itertools import repeat, chain
from json import loads
from pathlib import Path
from platform import system
from subprocess import DEVNULL, PIPE, STDOUT, Popen, check_output
from urllib import parse, request

from bottle import default_app, redirect
from bottle import request as brequest
from bottle import route, static_file, template
from waitress import serve

##### APP INFORMATION #####
application = default_app()
# config.ini in root directory
config = ConfigParser()
config.read("config.ini")
gset = config["General"]
lset = config["Linux"]
wset = config["Windows"]
# General Settings
server_port = int(gset["server_port"])
vid_msg_time = gset["vid_msg_time"]
refresh_freq = gset["refresh_freq"]
chat_params = gset["chat_params"]
# Linux Settings
vplayer = lset["vplayer"]
vplayer_args = lset["vplayer_args"]
# Windows Settings
vlc_path = wset["vlc_path"]
# Twitch Dev Client and API info
client_id = "o232r2a1vuu2yfki7j3208tvnx8uzq"
redirect_uri = "http://localhost:8080/authentication"
app_scopes = "clips:edit+user:edit+user:edit:follows+chat:edit+chat:read+whispers:read+whispers:edit"
helix = "https://api.twitch.tv/helix"
print("\nGo to http://localhost:8080 on your favorite browser")


####### Database Transactions #######
def db_conn():
    conn = sqlite3.connect("data.db")
    c = conn.cursor()
    return conn, c


def create_table(tname, values):
    c = db_conn()[1]
    vstring = ",".join([f"{value} text" for value in values])
    c.execute(f"create table if not exists {tname} ({vstring})")


def insert_in_table(tname, values):
    conn, c = db_conn()
    qmarks = f"({','.join('?'*len(values))})"
    with conn:
        c.execute(f"insert or ignore into {tname} values {qmarks}", values)


def fetch_from_table(query, fetchall=False):
    conn = db_conn()[0]
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute(f"select {query}")
    if fetchall:
        return c.fetchall()
    return c.fetchone()


def change_value(query):
    conn, c = db_conn()
    with conn:
        c.execute(f"{query}")


####### Initialization #######
def build_cache():
    create_table("games", ("box_art_url", "id", "name"))
    create_table("followed_streamers", ("id", "login", "name", "profile_image_url"))
    create_table("all_streamers", ("id", "login", "name", "profile_image_url"))
    streamer_cache_directory = "cache/streamer_profiles"
    Path(streamer_cache_directory).mkdir(exist_ok=True, parents=True)
    game_cache_directory = "cache/games"
    Path(game_cache_directory).mkdir(exist_ok=True, parents=True)
    follows = get_follows()
    with ThreadPoolExecutor() as tp:
        tp.map(streamer_cache, follows, repeat("followed_streamers"))
        tp.map(streamer_cache, follows, repeat("all_streamers"))


####### Caching #######
def streamer_cache(user_id, table):
    user = get_request(f"{helix}/users?id={user_id}")["data"][0]
    imglink = user["profile_image_url"].replace("300x300", "600x600")
    imgfile = f"cache/streamer_profiles/{user['display_name']}.jpg"
    request.urlretrieve(imglink, imgfile)
    insert_in_table(f"{table}", (user["id"], user["login"], user["display_name"], imgfile))


def game_cache(game):
    boxartlink = game["box_art_url"].rsplit("-", 1)[0] + ".jpg"
    boxart = f"cache/games/{game['id']}.jpg"
    request.urlretrieve(boxartlink, boxart)
    insert_in_table("games", (boxart, game["id"], game["name"]))


def filter_games_to_cache(games):

    cache = [row[0] for row in fetch_from_table("id from games", fetchall=True)]
    to_cache = [game for game in games if game["id"] not in cache]
    if to_cache:
        with ThreadPoolExecutor() as tp:
            tp.map(game_cache, to_cache)
    return [fetch_from_table(f"* from games where id = '{game['id']}'") for game in games]


def filter_channels_to_cache(channels):
    cache = [row[0] for row in fetch_from_table("id from all_streamers", fetchall=True)]
    to_cache = [channel["id"] for channel in channels if channel["id"] not in cache]
    if to_cache:
        with ThreadPoolExecutor() as tp:
            tp.map(streamer_cache, to_cache, repeat("all_streamers"))
    return [fetch_from_table(f"* from all_streamers where id='{channel['id']}'") for channel in channels]


####### HTTP Request GET #######
def headers():
    oauth = fetch_from_table("access_token from accounts")[0]
    return {"Client-ID": client_id, "Authorization": f"Bearer {oauth}"}


def get_request(url):
    req = request.Request(url, headers=headers())
    return loads(request.urlopen(req).read().decode("utf-8"))


def input_request(url, values, method):
    data = parse.urlencode(values).encode("ascii")
    pdheaders = headers()
    pdheaders["client-id"] = pdheaders.pop("Client-ID")
    req = request.Request(url, data=data, headers=pdheaders, method=method)
    request.urlopen(req)


####### Twitch API Follows #######
def get_follows():
    user_id = fetch_from_table("id from accounts")["id"]
    resp = get_all_pages(f"{helix}/users/follows?from_id={user_id}&first=100")
    follows = [follow["to_id"] for follow in resp]
    return follows


def get_live_followed():
    follows = fetch_from_table("id from followed_streamers", fetchall=True)
    follows = [follow["id"] for follow in follows]
    composite = [follows[x : x + 100] for x in range(0, len(follows), 100)]

    def composition(follows):
        user_ids = "&".join([f"user_id={follow}" for follow in follows])
        return [stream for stream in get_request(f"{helix}/streams?{user_ids}")["data"]]

    with ThreadPoolExecutor() as tp:
        streams = list(chain.from_iterable(tp.map(composition, composite)))
    return streams


def toggle_follow(to_id, state):
    from_id = fetch_from_table("id from accounts")[0]
    mode = "POST"
    if state:
        mode = "DELETE"
    url = f"{helix}/users/follows"
    values = {"to_id": to_id, "from_id": from_id}
    input_request(url, values, mode)


def update_follows_cache():
    cache = [row[0] for row in fetch_from_table("id from followed_streamers", fetchall=True)]
    follows = get_follows()
    to_cache = [streamer for streamer in follows if streamer not in cache]
    to_delete = [streamer for streamer in cache if streamer not in follows]
    if to_cache:
        with ThreadPoolExecutor() as tp:
            tp.map(streamer_cache, to_cache, repeat("followed_streamers"))
    if to_delete:
        [change_value(f"delete from followed_streamers where id = {streamer}") for streamer in to_delete]


####### Stream Data Organization #######
def stream_info(stream):
    if stream["game_id"] == "":
        stream["game_id"] = "0000"
    stream["game_name"], stream["game_boxart"] = game_info(stream["game_id"])
    stream["thumbnail_url"] = stream["thumbnail_url"].replace("-{width}x{height}", "")
    stream["uptime"] = time_elapsed(stream["started_at"])
    try:
        stream["profile_image_url"] = fetch_from_table(
            f"profile_image_url from all_streamers where id={stream['user_id']}"
        )[0]
    except:
        streamer_cache(stream["user_id"], "all_streamers")
        stream["profile_image_url"] = fetch_from_table(
            f"profile_image_url from all_streamers where id={stream['user_id']}"
        )[0]
    stream["login"] = fetch_from_table(f"login from all_streamers where id='{stream['user_id']}'")[0]
    return stream


def game_info(game_id):
    try:
        game_boxart, game_id, game_name = fetch_from_table(f"* from games where id={game_id}")
        return game_name, game_boxart
    except:
        if game_id == "0000":
            return (
                "Streaming",
                "https://static-cdn.jtvnw.net/ttv-static/404_boxart.jpg",
            )
        game_info = get_request(f"{helix}/games?id={game_id}")["data"][0]
        game_name = game_info["name"]
        boxartlink = game_info["box_art_url"].replace("-{width}x{height}", "")
        game_boxart = f"cache/games/{game_id}.jpg"
        request.urlretrieve(boxartlink, game_boxart)
        insert_in_table("games", (game_boxart, game_id, game_name))
        return game_name, game_boxart


####### Bottle Routes #######
@route("/")
def index():
    try:
        user = fetch_from_table("* from accounts")
    except sqlite3.OperationalError:
        print("\nNo user found... Follow the link to authenticate")
        url = f"https://id.twitch.tv/oauth2/authorize?client_id={client_id}&redirect_uri={redirect_uri}&response_type=token&scope={app_scopes}"
        return template("noUser.tpl", url=url)
    try:
        update_follows_cache()
    except:
        build_cache()
    return template("index", user=user)


@route("/authentication")
def authentication():
    if access_token := brequest.query.get("access_token"):
        req = request.Request(
            f"{helix}/users",
            headers={
                "Client-ID": client_id,
                "Authorization": f"Bearer {access_token}",
            },
        )
        user_account = loads(request.urlopen(req).read().decode("utf-8"))["data"][0]
        create_table(
            "accounts",
            (
                "id",
                "login",
                "display_name",
                "profile_image_url",
                "access_token",
            ),
        )
        insert_in_table(
            "accounts",
            (
                user_account["id"],
                user_account["login"],
                user_account["display_name"],
                user_account["profile_image_url"],
                access_token,
            ),
        )
        print(f"\nSuccess! Logged in as {user_account['display_name']}")
        return redirect("/")
    return template("authentication.tpl")


@route("/live")
def live():
    if channel := brequest.query.get("stream"):
        profile, base_url = watch_live_video(channel)
        return template(
            "playstream",
            base_url=base_url,
            profile=profile,
            vid_msg_time=vid_msg_time,
        )
    streams = get_live_followed()
    with ThreadPoolExecutor() as tp:
        streams = tp.map(stream_info, streams)
    streams = sorted(streams, key=lambda info: info["viewer_count"], reverse=True)
    return template(
        "live",
        streams=streams,
        refresh_freq=refresh_freq,
        chat_params=chat_params,
    )


@route("/following")
def following():
    followed_streamers = fetch_from_table("* from followed_streamers", fetchall=True)
    followed_streamers = sorted(followed_streamers, key=lambda streamer: streamer["name"].lower())
    return template("following", channels=followed_streamers)


@route("/channel/<channel>")
def channel(channel):
    try:
        channel = dict(fetch_from_table(f"* from followed_streamers where name = '{channel}'"))
        state = True
    except:
        state = False
        channel = dict(fetch_from_table(f"* from all_streamers where name = '{channel}'"))

    if brequest.query.get("follow"):
        toggle_follow(channel["id"], state)
        update_follows_cache()
        redirect(f"/channel/{channel['name']}")

    if brequest.query.get("vods"):
        querystring = brequest.query_string
        params = {item[0]: item[1] for item in [query.split("=") for query in querystring.split("&")]}
        vperiod = brequest.query.get("period")
        vsort = brequest.query.get("sort")
        vtype = brequest.query.get("type")
        url = f"{helix}/videos?user_id={channel['id']}&first=100&period={vperiod}&sort={vsort}&type={vtype}"
        vods = get_all_pages(url)
        for vod in vods:
            vod["thumbnail_url"] = vod["thumbnail_url"].replace("%{width}x%{height}", "640x320")
            if vod["thumbnail_url"] == "":
                vod["thumbnail_url"] = "https://vod-secure.twitch.tv/_404/404_processing_640x360.png"
            vod["created_at"] = time_elapsed(vod["created_at"], count_days=True)

        return template("vods.tpl", channel=channel, vods=vods, state=state, params=params)

    if brequest.query.get("clips"):
        querystring = brequest.query_string
        params = {item[0]: item[1] for item in [query.split("=") for query in querystring.split("&")]}
        started_at = f"{params['min']}T00:00:00Z"
        ended_at = f"{params['max']}T00:00:00Z"
        url = f"{helix}/clips?broadcaster_id={channel['id']}&first=100&started_at={started_at}&ended_at={ended_at}"
        clips = get_request(url)["data"]
        with ThreadPoolExecutor() as tp:
            clips = tp.map(process_clips, clips)
        clips = sorted(clips, key=lambda info: info["view_count"], reverse=True)
        return template(
            "clips.tpl",
            channel=channel,
            clips=clips,
            state=state,
            params=params,
        )

    if video := brequest.query.get("video"):
        global video_proc
        kill_process(video_proc)
        if system() == "Linux":
            video_proc = Popen(
                f"{vplayer} {vplayer_args} {video} best",
                shell=True,
                stdout=DEVNULL,
                stderr=STDOUT,
            )
        if system() == "Windows":
            video_proc = Popen(f"{vlc_path} {video}", stdout=DEVNULL, stderr=STDOUT)
        return template("playvideo.tpl", profile=channel, vid_msg_time=vid_msg_time)
    return template("channelpage.tpl", channel=channel, state=state, params={})


@route("/top/games")
def top_games():
    games = get_request(f"{helix}/games/top?first=100")["data"]
    games = filter_games_to_cache(games)
    return template("games.tpl", games=games)


@route("/top/games/<game>")
def top_game_streams(game):
    if channel := brequest.query.get("stream"):
        profile, base_url = watch_live_video(channel)
        return template(
            "playstream",
            base_url=base_url,
            profile=profile,
            vid_msg_time=vid_msg_time,
        )
    streams = get_request(f"{helix}/streams?game_id={game}")["data"]
    game = fetch_from_table(f"* from games where id='{game}'")
    with ThreadPoolExecutor() as tp:
        streams = tp.map(stream_info, streams)
    streams = sorted(streams, key=lambda info: info["viewer_count"], reverse=True)
    return template(
        "topstream.tpl",
        streams=streams,
        game=game,
        refresh_freq=refresh_freq,
        chat_params=chat_params,
    )


@route("/top/streams")
def top_streams():
    if channel := brequest.query.get("stream"):
        profile, base_url = watch_live_video(channel)
        return template(
            "playstream",
            base_url=base_url,
            profile=profile,
            vid_msg_time=vid_msg_time,
        )
    streams = get_request(f"{helix}/streams")["data"]
    with ThreadPoolExecutor() as tp:
        streams = tp.map(stream_info, streams)
    streams = sorted(streams, key=lambda info: info["viewer_count"], reverse=True)
    return template(
        "topstream.tpl",
        streams=streams,
        game=False,
        refresh_freq=refresh_freq,
        chat_params=chat_params,
    )


@route("/search")
def search():
    query = brequest.query.get("query")
    query_type = brequest.query.get("type")
    url = f"{helix}/search/{query_type}?query={parse.quote(query)}&first=5"
    data = get_request(url)["data"]
    results = filter_channels_to_cache(data) if query_type == "channels" else filter_games_to_cache(data)
    return template("search.tpl", query=query, results=results, query_type=query_type)


@route("<filename:path>")
def server_static(filename):
    return static_file(filename, root="./")


####### Utilities #######
def get_all_pages(url):
    results = []
    while True:
        response = get_request(url)
        data = response["data"]
        if data == []:
            break
        results += data
        if response["pagination"] == {}:
            return results
        pagination = response["pagination"]["cursor"]
        if "after" in url:
            url = url[: (url.rfind("=") + 1)] + pagination
        else:
            url = url + f"&after={pagination}"
    return results


def time_elapsed(start, count_days=None):
    start = datetime.strptime(start, "%Y-%m-%dT%H:%M:%SZ").replace(tzinfo=timezone.utc)
    current = datetime.now(tz=timezone.utc)
    elapsed = round((current - start).total_seconds())
    if count_days is True:
        days = elapsed // (24 * 3600)
        elapsed %= 24 * 3600
    hours = elapsed // 3600
    if count_days is True:
        if days >= 7:
            weeks = days // 7
            if weeks >= 4:
                months = weeks // 4
                if months >= 12:
                    years = months // 12
                    months %= 12
                    return f"{years} year(s), {months} month(s)"
                weeks %= 4
                return f"{months} month(s), {weeks} week(s)"
            days %= 7
            return f"{weeks} week(s), {days} day(s)"
        if days == 0:
            return f"{hours} hour(s)"
        return f"{days} day(s), {hours} hour(s)"
    elapsed %= 3600
    minutes = elapsed // 60
    return f"{hours}h{minutes}m"


def process_clips(clip):
    if clip["video_id"]:
        clip["vod_link"] = vod_from_clip(clip["created_at"], clip["video_id"])
    else:
        clip["vod_link"] = None
    clip["created_at"] = time_elapsed(clip["created_at"], count_days=True)
    if clip["game_id"]:
        clip["game_name"], clip["game_boxart"] = game_info(clip["game_id"])
    clip.setdefault("game_name", "Streaming")
    clip.setdefault("game_boxart", None)
    return clip


def vod_from_clip(clip_time, vod_id):
    url = f"{helix}/videos?id={vod_id}"
    vod_info = get_request(url)["data"][0]
    vod_time = datetime.strptime(vod_info["created_at"], "%Y-%m-%dT%H:%M:%SZ").replace(tzinfo=timezone.utc)
    clip_time = datetime.strptime(clip_time, "%Y-%m-%dT%H:%M:%SZ").replace(tzinfo=timezone.utc)
    elapsed = round((clip_time - vod_time).total_seconds() - 61)
    if "h" not in vod_info["duration"]:
        vod_info["duration"] = f"0h{vod_info['duration']}"
    vod_duration = datetime.strptime(vod_info["duration"], "%Hh%Mm%Ss") - datetime.strptime("00:00:00", "%H:%M:%S")
    vod_duration = vod_duration.total_seconds()
    if elapsed > vod_duration:
        return f"http://www.twitch.tv/videos/{vod_id}"
    hours = elapsed // 3600
    elapsed %= 3600
    minutes = elapsed // 60
    elapsed %= 60
    seconds = elapsed
    return f"http://www.twitch.tv/videos/{vod_id}/?t={hours}h{minutes}m{seconds}s"


def win_proc_exists(process_name):
    call = "TASKLIST", "/FI", "imagename eq %s" % process_name
    output = check_output(call).decode()
    last_line = output.strip().split("\r\n")[-1]
    return last_line.lower().startswith(process_name.lower())


####### OS video subprocess #######
video_proc = None


def watch_live_video(channel):
    global video_proc
    profile = fetch_from_table(f"* from all_streamers where login = '{channel}'")
    base_url = brequest.fullpath
    kill_process(video_proc)
    if system() == "Linux":
        video_proc = Popen(
            f"streamlink --player '{vplayer} {vplayer_args}' --twitch-low-latency twitch.tv/{channel} best",
            shell=True,
            stdout=DEVNULL,
            stderr=STDOUT,
        )
    if system() == "Windows":
        video_proc = Popen(
            f"streamlink --twitch-low-latency twitch.tv/{channel} best",
            stdout=DEVNULL,
            stderr=STDOUT,
        )
    return profile, base_url


def kill_process(video_proc):
    try:
        try:
            o, e = video_proc.communicate(timeout=1)
        except:
            video_proc.kill()
            o, e = video_proc.communicate()
    except:
        pass
    if system() == "Linux":
        if Popen(f"pgrep {vplayer}", shell=True, stdout=PIPE).communicate()[0] != b"":
            Popen(f"killall {vplayer}", shell=True, stdout=DEVNULL, stderr=STDOUT).wait()
    if system() == "Windows":
        if win_proc_exists("vlc.exe"):
            Popen("taskkill /IM vlc.exe /F", stdout=DEVNULL, stderr=STDOUT).wait()


if __name__ == "__main__":
    serve(application, host="localhost", port=server_port, threads=8)
