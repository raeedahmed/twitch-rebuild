% include('channelpage.tpl', title=f"{channel['name']} clips",state=state)
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        body {
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;
            background-color: rgba(18, 18, 18, 1);
        }

        .navbar {
            position: fixed;
            top: 0;
            width: 100%;
            background-color: #333;
        }

        .navbar a {
            float: left;
            font-size: 16px;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        .dropdown {
            float: left;
            overflow: hidden;
        }

        .dropdown .dropbtn {
            font-size: 16px;
            border: none;
            outline: none;
            color: white;
            padding: 14px 16px;
            background-color: inherit;
            font-family: inherit;
            margin: 0;
        }

        .navbar a:hover,
        .dropdown:hover .dropbtn {
            background-color: #9b24fd;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
        }

        .dropdown-content a {
            float: none;
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
            text-align: left;
        }

        .dropdown-content a:hover {
            background-color: #ddd;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .content {
            margin-top: 60px;
        }

        .navbar input[type=text] {
            float: right;
            padding: 6px;
            border: none;
            margin-top: 8px;
            margin-right: 16px;
            font-size: 17px;
        }

        .main {
            color: white;
            padding: 16px;
            margin-top: 30px;
        }
    </style>
</head>
<div class=main>
    <h2>Clips from {{params['min']}} to {{params['max']}}</h2>
    % for clip in clips:
    <h3>{{clip['title']}}</h3>
    <h4><img src={{f"/{clip['game_boxart']}" if clip['game_boxart'] else "https://static-cdn.jtvnw.net/ttv-static/404_boxart.jpg"}} width="50">{{clip['game_name']}}</h4>
    <a href="?video={{clip['url']}}"><img src="{{clip['thumbnail_url']}}" width=272></a>
    <h4>Created: {{clip['created_at']}} ago</h4>
    <h4>{{clip['view_count']}} views</h4>
    % if clip["vod_link"]:
    <a href="?video={{clip['vod_link']}}">View Vod</a>
    % end
    % end
</div>
