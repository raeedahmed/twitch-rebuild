% rebase('header.tpl', title='Playing Video')
<h1>Watching <img src="/{{profile['profile_image_url']}}" width="50"> {{profile['name']}} Video on Demand</h1>
<h2>Bringing you back shortly...</h2>
<script>
    setTimeout(function(){
      window.history.back()
    }, {{int(vid_msg_time)*1000}});
  </script>