<!-- <!DOCTYPE html> -->
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
  <style>
    body {
      margin: 0;
      font-family: "Roboto";
      background-color: rgba(18, 18, 18, 1);
    }

    .navbar {
      position: fixed;
      top: 0;
      width: 100%;
      background-color: #333;
      z-index: 9999;
    }

    .navbar a {
      float: left;
      font-size: 16px;
      color: white;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
    }

    .dropdown {
      float: left;
      overflow: hidden;
    }

    .dropdown .dropbtn {
      font-size: 16px;
      border: none;
      outline: none;
      color: white;
      padding: 14px 16px;
      background-color: inherit;
      font-family: inherit;
      margin: 0;
    }

    .navbar a:hover,
    .dropdown:hover .dropbtn {
      background-color: #9b24fd;
    }

    .dropdown-content {
      display: none;
      position: absolute;
      background-color: #f9f9f9;
      min-width: 160px;
      box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
      z-index: 1;
    }

    .dropdown-content a {
      float: none;
      color: black;
      padding: 12px 16px;
      text-decoration: none;
      display: block;
      text-align: left;
    }

    .dropdown-content a:hover {
      background-color: #ddd;
    }

    .dropdown:hover .dropdown-content {
      display: block;
    }

    .content {
      margin-top: 60px;
    }

    .navbar input[type=text],
    .search {
      float: right;
      padding: 6px;
      border: none;
      margin-top: 8px;
      margin-right: 16px;
      font-size: 17px;
    }

    .main {
      color: white;
      padding: 16px;
      margin-top: 30px;
      /* height: 1500px; Used in this example to enable scrolling */
    }

    section {
      display: flex;
      flex-wrap: wrap;
    }

    .stream, .video {

      width: 25%;
      vertical-align: top;
      display: flex;
      flex-direction: column;
      box-sizing: border-box;
      padding: 35px 10px 15px 10px;
      border: 1px solid black;
    }

    @media screen and (max-width: 1800px) {
      .stream, .video {
        width: 33%;
      }
    }

    @media screen and (max-width: 1280px) {
      .stream, .video {
        width: 50%;
      }
    }


    .title {
      overflow: hidden;
      text-overflow: ellipsis;
      display: inline-block;
      white-space: nowrap;
    }

    .thumbnail {
      display: block;
      position: relative;
      color: white;
    }

    .thumbnail:hover {
      box-shadow:
        1px 1px #b811eb,
        2px 2px #b811eb,
        3px 3px #b811eb,
        4px 4px #b811eb,
        5px 5px #b811eb,
        6px 6px #b811eb;
      -webkit-transform: translateX(-3px);
      transform: translateX(-3px);
      transition: .5s ease;
      transition: .3s ease;
    }

    .bottom-left {
      position: absolute;
      bottom: -20px;
      left: 15px;
      background-color: rgba(0, 0, 0, 0.6);
      margin-left: -5px;
      margin-bottom: 20px;
      padding-left: 5px;
      padding-right: 5px;
    }

    .bottom-right {
      position: absolute;
      bottom: -20px;
      right: 5px;
      background-color: rgba(0, 0, 0, 0.6);
      margin-right: 5px;
      margin-bottom: 20px;
      padding-left: 5px;
      padding-right: 5px;
    }

    .top-left {
      position: absolute;
      background-color: rgba(0, 0, 0, 0.6);
      top: -20px;
      left: 10px;
      margin-top: 20px;
      padding-left: 5px;
      padding-right: 5px;
    }

    .streamer_info {
      align-items: center;
      display: inline-flex;
    }

    .streamer_info a,
    div {
      padding-inline: 10px;
    }

    .button {
      border-radius: 4px;
      background-color: #9b24fd;
      border: none;
      color: #FFFFFF;
      text-align: center;
      font-size: 18px;
      padding: 5px;
      width: 100px;
      transition: all 0.5s;
      cursor: pointer;
      margin: 5px;
    }

    .button span {
      cursor: pointer;
      display: inline-block;
      position: relative;
      transition: 0.5s;
    }

    .button span:after {
      content: '\00bb';
      position: absolute;
      opacity: 0;
      top: 0;
      right: -20px;
      transition: 0.5s;
    }

    .button:hover span {
      padding-right: 25px;
    }

    .button:hover span:after {
      opacity: 1;
      right: 0;
    }
  </style>
</head>

<body>

  <div class="navbar">
    <a href="/">Home</a>
    <a href="/live">Live</a>
    <a href="/following">Following</a>
    <div class="dropdown">
      <button class="dropbtn">Browse
        <i class="fa fa-caret-down"></i>
      </button>
      <div class="dropdown-content">
        <a href="/top/games">Top Games</a>
        <a href="/top/streams">Top Streams</a>
      </div>
    </div>
    <form action="/search" method="get">
      <select id="type" name="type" class=search>
        <option selected value="channels">Channels</option>
        <option value="categories">Games</option>
      </select>
      <input type="text" name="query" placeholder="Search...">
    </form>
  </div>


  <div class="main">
    {{!base}}
    <title>{{title}}</title>
  </div>

</body>

</html>