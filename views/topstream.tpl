% rebase('header.tpl', title='Top Streams')
<meta http-equiv="refresh" content="{{refresh_freq}}">
% if game:
<h1><img src="/{{game['box_art_url']}}" width=100>Top Streams for {{game['name']}}</h1>
% else:
<h1>Top Streams</h1>
% end
<section>
    % for stream in streams:
    <div class="stream">
        <div class="streamer_info">
            <a href="/channel/{{stream['user_name']}}"><img style="align-content: center;"
                    src="/{{stream['profile_image_url']}}" width=100></a>
            <div>
                <h1>{{stream['user_name']}}</h1>
                % if game == False:
                <h5 class="title" title="{{stream['game_name']}}">{{stream['game_name']}}</h5>
                % end
            </div>
            % if game == False:
            <div>
                <h5><a href="/top/games/{{stream['game_id']}}"><img src="/{{stream['game_boxart'].replace(" ","%20")}}"
                            width=50></a></h5>
            </div>
            % end
        </div>
        <h4 class="title" title="{{stream['title']}}">{{stream['title']}}</h4>
        <div class="thumbnail">
            <a href="?stream={{stream['login']}}"><img src="{{stream['thumbnail_url']}}" width=100%></a>
            <h5 class="bottom-left">Uptime: {{stream['uptime']}}</h5>
            <h5 class="bottom-right">{{stream['viewer_count']}} viewers</h5>
        </div>
        <div>
            <a href="javascript: void(0)" onclick="window.open('https://twitch.tv/{{stream['login']}}/chat?popout=','customWindow', {{chat_params}}, '_blank')">Open Chat</a>
        </div>
    </div>
    % end
</section>