% rebase('header.tpl', title=f"Search Results for '{query}'")
<h2>Results for {{query}} in {{query_type.title()}}</h2>
% if query_type == "channels":
    % for result in results:
        <div>
            <h3><a href="/channel/{{result['name']}}"><img src="{{result['profile_image_url']}}"
                        width="100"></a>{{result['name']}}</h3>
        </div>
    % end
% else:
    % for result in results:
        <div>
            <h3><a href="/top/games/{{result['id']}}"><img src="{{result['box_art_url']}}"
                        width="100"></a>{{result['name']}}</h3>
        </div>
    % end