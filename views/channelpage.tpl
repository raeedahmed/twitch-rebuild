% rebase('header.tpl', title=channel['name'])
<h1>{{channel['name']}}</h1>
    <form action="" method="get" id=follow>
      <input type="submit" name="follow" value={{"Unfollow" if state else "Follow"}} />
    </form>
<section>
  <img src="/{{channel['profile_image_url']}}" width=200>
</section>


<section>
  <div>
    <form action="/channel/{{channel['name']}}" method="get" id="vodfilter">
      <label for="period">Period:</label>
      <select id="period" name="period">
        <option selected value="all">All</option>
        <option value="day">Day</option>
        <option value="week">Week</option>
        <option value="month">Month</option>
      </select>

      <label for="sort">Sort:</label>
      <select id="sort" name="sort">
        <option selected value="time">Time</option>
        <option value="trending">Trending</option>
        <option value="views">Views</option>
      </select>

      <label for="type">Type:</label>
      <select id="type" name="type">
        <option selected value="all">All</option>
        <option value="upload">Upload</option>
        <option value="archive">Archive</option>
        <option value="highlight">Highlight</option>
      </select>
      <button class="button" name="vods" value="vods"><span>View VODs </span></button>
    </form>
    <form action="/channel/{{channel['name']}}" method="get" id="clips">
      <label for="start">Start date:</label>
      <input type="date" id="start" name="min" value={{params['min'] if params.get('min') else ""}}>
      <label for="end">End date:</label>
      <input type="date" id="end" name="max" value={{params['max'] if params.get('max') else ""}}>
      <button class="button" name="clips" value="clips"><span>View Clips </span></button>
    </form>
  </div>

</section>