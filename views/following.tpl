% rebase('header.tpl', title='Followed Channels')
<style>
  section {
    flex-direction: row;
  }
  .channel {
    box-sizing: border-box;
  }
</style>
<h3>Following {{len(channels)}} streamers</h3>
<section>
  % for channel in channels:
  <div class="channel">
    <a href="/channel/{{channel['name']}}"><img src={{channel['profile_image_url']}} width=100></a>
    <h4>{{channel['name']}}</h4>
  </div>
  % end
</section>




