% rebase('header.tpl',title=f"{channel['name']} VODs")
<h1><img src="/{{channel['profile_image_url']}}" width=200>{{channel['name']}}</h1>

<div>
    <form action="" method="get" id=follow>
        <input type="submit" name="follow" value={{"Unfollow" if state else "Follow"}} />
    </form>
</div>



<form action="/channel/{{channel['name']}}" method="get" id="vodfilter">
    <label for="period">Period:</label>
    <select id="period" name="period">
        <option selected value="all">All</option>
        <option value="day">Day</option>
        <option value="week">Week</option>
        <option value="month">Month</option>
    </select>

    <label for="sort">Sort:</label>
    <select id="sort" name="sort">
        <option selected value="time">Time</option>
        <option value="trending">Trending</option>
        <option value="views">Views</option>
    </select>

    <label for="type">Type:</label>
    <select id="type" name="type">
        <option selected value="all">All</option>
        <option value="upload">Upload</option>
        <option value="archive">Archive</option>
        <option value="highlight">Highlight</option>
    </select>
    <button class="button" name="vods" value="vods"><span>View VODs </span></button>
</form>


<form action="/channel/{{channel['name']}}" method="get" id="clips">
    <label for="start">Start date:</label>
    <input type="date" id="start" name="min" value={{params['min'] if params.get('min') else ""}}>
    <label for="end">End date:</label>
    <input type="date" id="end" name="max" value={{params['max'] if params.get('max') else ""}}>
    <button class="button" name="clips" value="clips"><span>View Clips </span></button>
</form>

<h2>{{params['type'].title()}} videos by {{params['sort'].title()}} from time: {{params['period'].title()}}</h2>
<section>
    % for vod in vods:
    <div class="video">
        <h4 class="title" title="{{vod['title']}}">{{vod['title']}}</h4>
        % if vod['description']:
        <h4 class="title" , title="{{vod['description']}}">{{vod['description']}}</h4>
        % end
        <div class="thumbnail">
            <a href="?video={{vod['url']}}"><img src="{{vod['thumbnail_url']}}" width=100%></a>
            <h4 class="top-left">Created: {{vod['created_at']}} ago</h4>
            <h4 class="bottom-right">Duration: {{vod['duration']}}</h4>
            <h4 class="bottom-left">{{vod['view_count']}} views</h4>
        </div>
    </div>
    % end

</section>