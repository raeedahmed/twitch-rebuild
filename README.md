# TwitchBottle

A single file Python script which provides a lightweight web interface to the official [Twitch](https://twitch.tv) website as a wrapper for the [Streamlink API](https://streamlink.github.io/), which fetches direct video streams from their respective plugin's URLs into the user's local video player. There are no external dependencies in the script except for streamlink itself. The web interface is powered by the excellent [BottlePy micro web-framework](https://bottlepy.org/docs/dev/) which itself is a single file that depends only the Python Standard Library.

## Features

* Compatibility across various platforms
* Browse top streams with game and search filters
* View followed channels and their streams
* Follow/Unfollow channels
* View filtered VODs and Clips from channels
* High quality streaming playback with low-latency via streamlink

TwitchBottle is built with the new [Twitch Helix API](https://dev.twitch.tv/docs/change-log) which still has many features from older versions to be rolled out

## Installation

### Install a supported video player

* Supported video player
  * Windows
    * [VLC media player](https://www.videolan.org/vlc/#download) (recommended)
    * [MPC-HC](https://github.com/clsid2/mpc-hc/releases)
  * Linux
    * [mpv](https://mpv.io/installation/) (recommended)
    * [VLC media player](https://www.videolan.org/vlc/#download)
    * [mplayer](http://mplayerhq.hu/design7/news.html)

### Run From Source

* (Windows) Download latest version of Python [here](https://www.python.org/downloads/) and add Python to PATH during the installation
* Download this project using the Download button or if you have git installed: `git clone https://gitlab.com/ruahmed/twitch-rebuild.git`

It is recommended to use a virtual environment. To do so, navigate to the project directory `twitch-rebuild/`

Create a virtual environment named 'venv' using the command:

```bash
python -m venv venv
```

In the project root directory where you can see the newly create venv/ directory,

* Windows: type the following and hit Enter:

  ```cmd
  venv\Scripts\activate.bat
  ```

* Linux:

  ```bash
  source venv/bin/activate
  ```

Install the dependencies listed in `requirements.txt`

  ```bash
  pip install -r requirements.txt
  ```

Run the script!

```bash
python TwitchBottle.py
```

## Usage

Start the program by clicking on the executable or running the script directly (as detailed in "Running from source")
You will be greeted with a terminal prompt to go to the webserver url. Default is `http://localhost:8080`

On first launch you must authenticate via twitch.tv's Oauth in your web browser.

![No User](ref/NoUserPrompt.png)

Upon successful login there may be some delay during initialization but you will be greeted with the homepage:

![Homepage](ref/Success.png)

An SQLite file called `data.db` will be created as well as a `cache` directory where all images are stored


## Built With

* [BottlePy](https://bottlepy.org/docs/dev/) - Micro framework
* [Streamlink](https://streamlink.github.io/) - Video streaming plugin

## Authors

* **Raeed Ahmed**

## Acknowledgments

* [Jivan Kulkarni](https://gitlab.com/jivank) for inspiring me to learn coding and giving me helpful pointers
* Pouya Bavafa for helping me along this project
